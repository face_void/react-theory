import React, {Component} from 'react';

import Car from "./Car/Car";
import ErrorBoundary from "./ErrorBoundary/ErrorBoundary";
import Counter from "./Counter/Counter";

import './App.scss';

export const ContextClicked = React.createContext(false);

class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            clicked: false,
            cars: [
                {name: 'Ford', year: 2018},
                {name: 'Audi', year: 2010},
                {name: 'Mazda', year: 2016},
            ],
            pageTitle: 'React components',
            showCars: false,
        };
    }

    handleToggleCars = () => {
        this.setState({
            showCars: !this.state.showCars,
        });
    }

    handleChangeName(name, index) {
        const car = this.state.cars[index];
        car.name = name;
        const cars = [...this.state.cars];
        this.setState({cars});
    }

    handleDelete(index) {
        const cars = this.state.cars.concat();
        cars.splice(index, 1);
        this.setState({cars});
    }

    render() {
        const divStyle = {
            textAlign: 'center',
        };

        let cars = null;

        if (this.state.showCars) {
            cars = this.state.cars.map((car, index) => {
                return (
                    <ErrorBoundary key={index}>
                        <Car
                            index={index}
                            name={car.name}
                            year={car.year}
                            onDelete={this.handleDelete.bind(this, index)}
                            onChangeName={event => this.handleChangeName(event.target.value, index)}
                        />
                    </ErrorBoundary>
                );
            });
        }

        return (
            <div style={divStyle}>
                {/*<h1>{this.state.pageTitle}</h1>*/}
                <h1>{this.props.title}</h1>

                <ContextClicked.Provider value={this.state.clicked}>
                    <Counter/>
                </ContextClicked.Provider>

                <hr/>
                <button
                    className={'AppButton'}
                    onClick={this.handleToggleCars}>Toggle cars</button>
                <button onClick={() => {this.setState({clicked: true})}}>Change clicked</button>
                <div style={{
                    width: 400,
                    margin: 'auto',
                    paddingTop: 20
                }}>
                    {cars}
                </div>
            </div>
        );
    }
}

export default App;
