import React from 'react';
import {ContextClicked} from "../App";

const Counter2 = props => {
    return (
        <div style={{
            border: '1px solid #ccc',
            width: 200,
            margin: '10px auto',
        }}>
            <h3>Counter 2</h3>
            <ContextClicked.Consumer>
                {clicked => clicked ? <p>Clicked</p> : null}
            </ContextClicked.Consumer>
        </div>
    );
}

export default Counter2;