import React from 'react';
import withClass from "../hoc/withClass";
import PropTypes from 'prop-types';

import './Car.scss'

class Car extends React.Component {
    constructor(props) {
        super(props);

        this.inputRef = React.createRef();
    }

    componentDidMount() {
        if (this.props.index === 1)
            this.inputRef.current.focus();
    }

    render() {
        const inputClasses = ['input'];

        if (this.props.name !== '')
            inputClasses.push('green');
        else
            inputClasses.push('red');

        if (this.props.name.length > 4)
            inputClasses.push('bold');

        return (
            <React.Fragment>
                <h3>Car name: {this.props.name}</h3>
                <p><b>Year: {this.props.year}</b></p>
                <input
                    ref={this.inputRef}
                    type="text"
                    className={inputClasses.join(' ')}
                    value={this.props.name}
                    onChange={this.props.onChangeName}
                />
                <button onClick={this.props.onDelete}>Delete</button>
            </React.Fragment>
        );
    }
}

Car.propTypes = {
    name: PropTypes.string,
    year: PropTypes.number,
    index: PropTypes.number,
    onChangeName: PropTypes.func,
    onDelete: PropTypes.func,
};

export default withClass(Car, 'Car')